﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SLuaDoc
{
    public class DocEntry
    {
        private int _lineNumber;
        private string _lineContents;
        private string _commentBrief;
        private List<string> _commentParams = new List<string>();
        private List<string> _commentReturns = new List<string>();

        public bool IsReady()
        {
            return (_commentParams.Count != 0) && (_commentReturns.Count != 0) && (_commentBrief != null);
        }

        #region setters
        public void SetLineNumber(int inLineNumber)
        {
            _lineNumber = inLineNumber;
        }

        public void SetLineContents(string inLineContents)
        {
            _lineContents = inLineContents;
        }

        public void SetBrief(string inBrief)
        {
            _commentBrief = inBrief;
        }

        public void AddParam(string inParams)
        {
            _commentParams.Add(inParams);
        }

        public void AddReturn(string inReturn)
        {
            _commentReturns.Add(inReturn);
        }
        #endregion

        #region getters
        public int GetLineNumber()
        {
            return _lineNumber;
        }

        public string GetLineContents()
        {
            return _lineContents;
        }

        public string GetBrief()
        {
            return _commentBrief;
        }

        public string[] GetParams()
        {
            return _commentParams.ToArray();
        }

        public string[] GetReturns()
        {
            return _commentReturns.ToArray();
        }
        #endregion

        public void Print()
        {
            Console.WriteLine("--- Line number " + GetLineNumber() + " ---");
            Console.WriteLine("Brief: " + GetBrief());

            var sParams = GetParams();
            for (int i = 0; i < sParams.Count(); i++)
            {
                Console.WriteLine("Params (" + i + ") " + sParams[i]);
            }

            var sReturns = GetReturns();
            for (int i = 0; i < sReturns.Count(); i++)
            {
                Console.WriteLine("Returns (" + i + ") " + sReturns[i]);
            }
        }
    }
}
