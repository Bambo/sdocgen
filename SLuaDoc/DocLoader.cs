﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SLuaDoc
{
    public static class DocLoader
    {
        public static List<DocEntry> LoadFile(string path)
        {
            List<DocEntry> _entries = new List<DocEntry>();
            DocEntry entry = new DocEntry();

            using (StreamReader reader = new StreamReader(path))
            {
                int lineNumber = 1;

                while (reader.EndOfStream == false)
                {
                    string currentLine = reader.ReadLine();

                    // All instructions to the documentor begin with --!
                    if (currentLine.StartsWith("--!"))
                    {
                        // Chop off un-needed --!
                        currentLine = currentLine.Substring(3);

                        // Chop off whitespace between --! and @NAME
                        while (currentLine[0] == ' ')
                        {
                            currentLine = currentLine.Substring(1);
                        }

                        // What type of DInstruction are we performing?
                        int diStart = currentLine.IndexOf("@", StringComparison.Ordinal) + 1;
                        int diEnd = currentLine.IndexOf(" ", StringComparison.Ordinal) - 1;
                        string diType = currentLine.Substring(diStart, diEnd);

                        // Contents of the DInstruction.
                        string diContents = currentLine.Substring(diEnd + 1);
                        while (diContents[0] == ' ')
                        {
                            diContents = diContents.Substring(1);
                        }


                        switch (diType)
                        {
                            case "brief":
                                entry.SetBrief(diContents);
                                break;
                            case "param":
                                entry.AddParam(diContents);
                                break;
                            case "returns":
                                entry.AddReturn(diContents);
                                break;
                        }

                        if (entry.IsReady())
                        {
                            // Finished with this entry.
                            // Where does it point to in the file?
                            entry.SetLineNumber(lineNumber);
                            _entries.Add(entry);

                            // Prepare for next data.
                            entry = new DocEntry();
                        }
                    }

                    lineNumber++;
                }
            }

            return _entries;
        }
    }
}
